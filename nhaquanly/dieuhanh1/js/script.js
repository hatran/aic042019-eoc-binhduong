﻿var lat = 10.92989;
var lon = 106.72401;
var map = new L.Map('map', {

    zoom: 20,
    minZoom: 4,
});



// create a new tile layer
var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    layer = new L.TileLayer(tileUrl, {
        attribution: '', //'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
        maxZoom: 16
    });

// add the layer to the map
map.addLayer(layer);

var cuuthuong = getParameterByName('ct');
var cuuhoa = getParameterByName('ch');
var chihuy = getParameterByName('chh');

// var lstPolice = [{
//     name: 'Công an phường Đông Ngạc',
//     lat: '21.0863999',
//     lon: '105.78120060000003',
//     address: 'Ngõ 35 Đông Ngạc, Đông Ngạc, Bắc Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Thụy Phương',
//     lat: '21.0925584',
//     lon: '105.77541229999997',
//     address: 'Thụy Phương, Thuỵ Phương, Từ Liêm, Hà Nội'
// },{
//     name: 'Công an phường Phú Thượng',
//     lat: '21.0883183',
//     lon: '105.80889520000005',
//     address:'379 Lạc Long Quân, Phú Thượng, Tây Hồ, Hà Nội'
// }];

var lstFirefighter = [{
    name: 'Đội PCCC Khu Công Nghiệp VSIP 1',
    lat: '10.9278',
    lon: '106.7352',
    address: 'An Phú, Thuận An, Bình Dương, Việt Nam'
}, {
    name: 'Cảnh Sát Pccc Số 2 TX.Thuận An',
    lat: '10.92516',
    lon: '106.73451',
    address: 'An Phú, Thuận An, Bình Dương, Việt Nam'
}]

var lstHospital = [{
    name: 'Bệnh viện đa khoa Thị xã Thuận An',
    lat: '10.90892',
    lon: '106.70192',
    address: 'Thị xã Thuận An, Nguyễn Văn Tiết, Bình Hòa, An Thạnh, An Đông, Thị xã Thuận An, Tỉnh Bình Dương, Việt Nam'
}, {
    name: 'Bệnh viện đa khoa Quốc Tế Miền Đông',
    lat: '10.90357',
    lon: '106.71034',
    address: 'Nguyễn Trãi, Bình Hòa, An Thạnh, An Đông, Thị xã Thuận An, Tỉnh Bình Dương, Việt Nam'
}]

// var lstCSGT = [{
//     name: 'Đội Cảnh Sát Giao Thông Thị Trấn Đông Anh',
//     lat: '21.174844',
//     lon: '105.846972',
//     address: 'QL3, Đông Anh, Hà Nội'
// },{
//     name: 'Đội CSGT Số 2',
//     lat: '21.061890',
//     lon: '105.808706',
//     address: '8A Xuân La, Tây Hồ, Hà Nội'
// }]


var xethang1 = [
    [lstFirefighter[0].lat, lstFirefighter[0].lon],
    [10.92780,106.73306],
    [10.92783,106.72829],
    [10.92785,106.72399],
    [lat, lon]
];
var xethang2 = [
    [lstFirefighter[1].lat, lstFirefighter[1].lon],
    [10.92517,106.72832],
    [10.92467,106.72811],
    [10.92473,106.72402],
    [lat, lon]
];

var xecuuthuong1 = [
    [lstHospital[0].lat, lstHospital[0].lon],
    [10.9102,106.7027],
    [10.91323,106.70596],
    [10.91577,106.70840],
    [10.92120,106.71363],
    [10.9209,106.7136],
    [10.9211,106.7154],
    [10.9230,106.7190],
    [10.9246,106.7202],
    [10.92474,106.72402],
    [lat, lon]
];
var xecuuthuong2 = [
    [lstHospital[1].lat, lstHospital[1].lon],
    [10.90293,106.71025],
    [10.90287,106.71073],
    [10.90266,106.71115],
    [10.90143,106.70961],
    [10.90117,106.71173],
    [10.90201,106.71216],
    [10.90373,106.71266],
    [10.9209,106.7136],
    [10.9211,106.7154],
    [10.9230,106.7190],
    [10.9246,106.7202],
    [10.92474,106.72402],
    [lat, lon]
];


var londonBrusselFrankfurtAmsterdamLondon = [
    [51.507222, -0.1275],
    [50.85, 4.35],
    [50.116667, 8.683333],
    [52.366667, 4.9],
    [51.507222, -0.1275]
];

var barcelonePerpignanPauBordeauxMarseilleMonaco = [
    [41.385064, 2.173403],
    [42.698611, 2.895556],
    [43.3017, -0.3686],
    [44.837912, -0.579541],
    [43.296346, 5.369889],
    [43.738418, 7.424616]
];


//map.fitBounds(mymap);

map.setView([lat, lon], 14);

var FireIcon = L.icon({
    iconUrl: 'images/fire-2-32_2.gif',
    iconSize: [36, 36], // size of the icon
});

var CCIcon = L.icon({
    iconUrl: 'images/Ol_icon_blue_example.png',
    iconSize: [32, 32], // size of the icon
});

var CTIcon = L.icon({
    iconUrl: 'images/Ol_icon_red_example.png',
    iconSize: [32, 32], // size of the icon
});

var CHIcon = L.icon({
    iconUrl: 'images/jeep.png',
    iconSize: [32, 32], // size of the icon
});

var marker = L.marker([lat, lon], { icon: FireIcon }).bindPopup('<p style="font-size:20px;">Hiện Trường: <span style="color:blue">Khu Công Nghiệp VSIP I</span></p>').addTo(map);
marker.openPopup();


if (cuuhoa == 1) {
    //========================================================================
    var marker1 = L.Marker.movingMarker(xethang1,
        [10000, 10000, 10000, 10000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker1.loops = 0;
    marker1.bindPopup();
    marker1.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe thang 1 thuộc: ' + lstFirefighter[0].name + '<br> Còn <strong>8</strong> phút nữa hiện trường')
            .setContent('Xe thang 1 <br> Còn <strong>8</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker1.start();

    //========================================================================

    var marker2 = L.Marker.movingMarker(xethang2,
        [10000, 10000, 10000, 10000], { autostart: false, loop: true, icon: CCIcon }).addTo(map);

    marker2.loops = 0;
    marker2.bindPopup();
    marker2.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe thang 2 thuộc: ' + lstFirefighter[1].name + '<br> Còn <strong>30</strong> phút nữa hiện trường')
            .setContent('Xe thang 2 <br> Còn <strong>30</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    marker2.start();
}

if (cuuthuong == 1) {
    var markerCT1 = L.Marker.movingMarker(xecuuthuong1,
        [10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    markerCT1.loops = 0;
    markerCT1.bindPopup();
    markerCT1.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
            .setContent('Xe cứu thương 1 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    markerCT1.start();

    var markerCT2 = L.Marker.movingMarker(xecuuthuong2,
        [10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000, 10000], { autostart: false, loop: true, icon: CTIcon }).addTo(map);

    markerCT2.loops = 0;
    markerCT2.bindPopup();
    markerCT2.on('mouseover', function(e) {
        //open popup;
        var popup = L.popup()
            .setLatLng(e.latlng)
            // .setContent('Xe cứu thương 1 thuộc: ' + lstHospital[2].name + '<br> Còn <strong>10</strong> phút nữa hiện trường')
            .setContent('Xe cứu thương 1 <br> Còn <strong>10</strong> phút nữa đến hiện trường')
            .openOn(map);
    });
    markerCT2.start();


}

var route1 = new L.Routing.control({
    waypoints: [
        L.latLng(lstFirefighter[0].lat, lstFirefighter[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "red", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route2 = new L.Routing.control({
    waypoints: [
        L.latLng(lstFirefighter[1].lat, lstFirefighter[1].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "blue", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route3 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[0].lat, lstHospital[0].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "#009900", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

var route4 = new L.Routing.control({
    waypoints: [
        L.latLng(lstHospital[1].lat, lstHospital[1].lon),
        L.latLng(lat, lon)
    ],
    addWaypoints: false,
    lineOptions: {
        styles: [{ color: "#990000", opacity: 1, weight: 5 }]
    },
    createMarker: function() { return null; },
    draggableWaypoints: false,
    routeWhileDragging: true
}).addTo(map);

// if (chihuy == 1)
// {
//     var markerCH = L.marker([lat - 0.02, lon - 0.02], { icon: CHIcon }).addTo(map);
// }

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}